import boto3
import botocore
from botocore.exceptions import ClientError
from config import AWS_ACCESS_KEY, AWS_SECRET_KEY, BUCKET
import csv
import io
from datetime import datetime
import requests


session = boto3.Session(
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_KEY,
    region_name="us-east-1",
)

s3 = session.client("s3")


class ContentBody:
    def get_content(self):
        raise NotImplementedError


class Sources:
    def __init__(self, url: str, api_key: str, filter: dict):
        self._url = url
        self._api_key = api_key
        self._filter = filter
        self.headers = self._make_headers()
        self.sources = self._make_sources()

    def _make_headers(self):
        header = {"Accept": "application/json", "X-Api-Key": self._api_key}
        return header

    def _request_sources(self):
        headers = self.headers
        response = requests.get(self._url, headers=headers, params=self._filter)
        response.raise_for_status()
        return response.json()["sources"]

    def _make_sources(self):
        return {item["id"]: item["url"] for item in self._request_sources()}


class CSVMaker(ContentBody):
    def make_content(self, content):
        source = content.source[content.inner_key]
        fields = list(source[0].keys())
        buffer = io.StringIO()
        try:
            writer = csv.DictWriter(buffer, fieldnames=fields)
            writer.writeheader()
            for item in source:
                writer.writerow(item)
            return buffer.getvalue()
        finally:
            buffer.close()


class AWSwriter:
    def __init__(self, source, source_id, key="articles", bucket=BUCKET, session=s3, content_maker=CSVMaker()):
        self._bucket = bucket
        self._s3 = session
        self._content_maker = content_maker
        self.source = source
        self.inner_key = key
        self._source_id = source_id

    def _make_content(self):
        return self._content_maker.make_content(self)

    def put_in_bucket(self):
        timestamp = str(datetime.now()).replace(".", ":")
        key = f"{self._source_id}/{timestamp}_headlines.csv"
        body = self._make_content()
        try:
            self._s3.put_object(Body=body, Bucket=self._bucket, Key=key)
        except botocore.exceptions.ClientError as e:
            print(e)
            pass
