import os
from dotenv import load_dotenv

load_dotenv()


API_KEY = os.environ.get("API_KEY")
MAIN_URL = "https://newsapi.org"
SOURCES_URL = MAIN_URL + "/v2/top-headlines/sources"
TOP_HEADLINES_URL = MAIN_URL + "/v2/top-headlines"
AWS_ACCESS_KEY = os.environ.get("AWS_ACCESS_KEY")
AWS_SECRET_KEY = os.environ.get("AWS_SECRET_KEY")
BUCKET = os.environ.get("BUCKET")
FILTER = {
    "language": "en"
}