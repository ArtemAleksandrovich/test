import asyncio
import time
from config import API_KEY, SOURCES_URL, TOP_HEADLINES_URL, FILTER
import aiohttp
from common import Sources, AWSwriter
import concurrent.futures

executor = concurrent.futures.ThreadPoolExecutor()


async def process():
    sources = Sources(url=SOURCES_URL, api_key=API_KEY, filter=FILTER)
    tasks = []

    async with aiohttp.ClientSession(raise_for_status=True) as session:
        for source_id in sources.sources:
            task = asyncio.create_task(process_source_headliners(source_id, session, sources.headers))
            tasks.append(task)
        await asyncio.gather(*tasks)


async def process_source_headliners(source_id, session, headers):
    async with session.get(TOP_HEADLINES_URL, headers=headers, params={"sources": source_id}, ssl=False) as response:
        data = await response.json()
        writer = AWSwriter(data, source_id)
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(executor, writer.put_in_bucket)


if __name__ == "__main__":
    start_time = time.time()
    asyncio.run(process())
    print("--- %s seconds ---" % (time.time() - start_time))
